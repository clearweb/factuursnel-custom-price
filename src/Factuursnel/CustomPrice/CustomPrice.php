<?php namespace Factuursnel\CustomPrice;

use Eloquent;

class CustomPrice extends Eloquent
{
    protected $table = 'client_product_custom_price';
    protected $fillable = array('client_id', 'product_id', 'custom_price');


   	public function client()
    {
        return $this->belongsTo('\Factuursnel\Client\Client');
    }
    
    public function product()
    {
        return $this->hasMany('\Factuursnel\Product\Product');
    }
}
