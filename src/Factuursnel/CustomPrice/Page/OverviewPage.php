<?php namespace Factuursnel\CustomPrice\Page;

use \Clearweb\Clearwebapps\Layout\OverviewLayout;

use Factuursnel\Client\Client;

use Factuursnel\CustomPrice\Widget\CustomPriceListWidget;
use Factuursnel\CustomPrice\Widget\CustomPriceFormWidget;

class OverviewPage extends \Page\Page
{
	public function __construct() {
		$this->setSlug(trans('nav.custom_prices'));
	}

	function init() {
		$this->setLayout(new OverviewLayout);

		$client = Client::find($this->getParameter('client_id', 0));

		if (empty($client)) {
			\App::abort(404);
		}

		$this->setTitle(ucfirst(trans('app.customprices_for', array('client'=>$client->name))));

		$customPriceList = new CustomPriceListWidget;

		$this->addWidgetLinear(new CustomPriceFormWidget, 'right', 1)
        	 ->addWidgetLinear($customPriceList, 'left');

		return parent::init();
	}
}
