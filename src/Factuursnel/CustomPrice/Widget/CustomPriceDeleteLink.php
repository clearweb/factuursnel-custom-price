<?php namespace Factuursnel\CustomPrice\Widget;

use Clearweb\Clearwebapps\Eloquent\DeleteActionLink;

class CustomPriceDeleteLink extends DeleteActionLink
{
    public function getModelClass()
    {
        return '\Factuursnel\CustomPrice\CustomPrice';
    }
    
    public function init()
    {
        $this->setDeleteAction(new CustomPriceDelete);
        
        return parent::init();
    }
}
