<?php namespace Factuursnel\CustomPrice\Widget;

use Clearweb\Clearwebapps\Eloquent\DeleteAction;

class CustomPriceDelete extends DeleteAction
{
    public function getModelClass()
    {
        return '\Factuursnel\CustomPrice\CustomPrice';
    }
    
    
}
