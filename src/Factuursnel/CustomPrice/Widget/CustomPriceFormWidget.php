<?php namespace Factuursnel\CustomPrice\Widget;

use Clearweb\Clearworks\Communication\ParameterChanger;

use Clearweb\Clearwebapps\Eloquent\FormWidget;

use Clearweb\Clearwebapps\Form\Form;
use Clearweb\Clearwebapps\Form\Validator;

use Clearweb\Clearwebapps\Form\TextField;
use Clearweb\Clearwebapps\Form\HiddenField;
use Clearweb\Clearwebapps\Form\SelectField;
use Clearweb\Clearwebapps\Form\SubmitField;

use Factuursnel\CustomPrice\CustomPrice;
use Factuursnel\Product\Product;

class CustomPriceFormWidget extends FormWidget
{
    function getName()
    {
        return 'new_customprice';
    }
    
    function getModelClass()
    {
        return '\Factuursnel\CustomPrice\CustomPrice';
    }
    
    public function init()
    {
        parent::init();
        
        $this->addStyle('/invoices/css/make_order.css');
        
        $clientId = $this->getParameter('client_id');

        $this->getForm()
            ->replaceField( 'client_id', with(new HiddenField)->setName('client_id')->setValue($clientId) )
            ->replaceField('submit', with(new SubmitField())->setName('add'));
            ;

        $this->getForm()
        	->removeField('client_id');
        
        return $this;
    }

    public function submit(array $post)
    {
        if (empty($post['id'])) {
            $values = array();
            foreach($post as $k=>$v) { $values[$k] = $v; }
            $values['client_id'] = $this->getParameter('client_id');
            $post = $values;

            $this->getContainer()
            	->addViewable(with(new ParameterChanger())->setParameter('client_id', $post['client_id']))
            ;
        }
		
		parent::submit($post);
    }
    
    public function execute()
    {
        parent::execute();
        
        $this->getContainer()->addClass('customprice-form');
    }
}
