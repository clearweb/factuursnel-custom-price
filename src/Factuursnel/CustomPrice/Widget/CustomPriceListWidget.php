<?php namespace Factuursnel\CustomPrice\Widget;

use Clearweb\Clearwebapps\Eloquent\ListWidget;

use Factuursnel\CustomPrice\CustomPrice;
use Factuursnel\Client\Client;

class CustomPriceListWidget extends ListWidget
{
	private $showClient = false;
	
    public function getModelClass()
    {
        return '\Factuursnel\CustomPrice\CustomPrice';
    }
    
    public function init()
    {
        parent::init();
        
		$this->setShowNewButton(true)
			->addQueryListener('client_id')
            ->setOrderAttribute('created_at')
            ->setOrderDirection('desc')
			;
        
        $this->getList()
            ->addColumn(
                        'price',
                        function ($row) {
                            $customPrice = CustomPrice::find($row['id']);
                            return '<div style="text-align:right">&euro; '.number_format($customPrice->custom_price, 2, ',', '.').'</div>';
                        },
                        4
                        )
            ->addActionLink(new CustomPriceDeleteLink)
            ;
        
        $client = Client::find($this->getParameter('client_id', 0));
        
        if ( ! empty($client)) {
            $this->getList()->removeColumn('client');
            $this->getList()->removeColumn('custom_price');
        }
        
		
        return $this;
    }
    
    protected function applyQueryFilters(\Illuminate\Database\Eloquent\Builder $builder) {
        $builder = parent::applyQueryFilters($builder);
        
        //$builder->where('status', Order::STATUS_PUBLISHED);
        
        return $builder;
    }
}
